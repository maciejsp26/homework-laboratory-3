/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package jsf;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author maciej
 */
@Named(value = "logowanie")
@RequestScoped
public class logowanie {
    
    private String nazwa;
    private String hasło;
    
    public String getNazwa()
    {
        return nazwa;
    }
    
    public void setNazwa(String nazwa) 
    {
        this.nazwa = nazwa;
    }
    public String getHasło() 
    {
        return hasło;
    }
    public void setHasło(String hasło) 
    {
        this.hasło = hasło;
    }
    
    public Boolean sprawdz()
    {
        return nazwa.equals("student") && hasło.equals("wcy");
    }
    
}
