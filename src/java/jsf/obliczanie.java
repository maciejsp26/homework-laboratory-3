/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package jsf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author maciej
 */
@Named(value = "obliczanie")
@RequestScoped
public class obliczanie {
    
    private String counter;
    List<Integer> list = new ArrayList<Integer>();
    Random rand = new Random();
    int ii;
    
    public String getCounter()
    {
        return counter;
    }
    
    public void setCounter(String counter) 
    {
        this.counter = counter;
    }
    
    public List<Integer> losowanie()
    {
        counter="10";
        int x = Integer.parseInt(counter);
        for(int i=0; i<x; i++)
        {
            list.add(rand.nextInt(1000));
        }
        return list;
    }
    
    public int losowanie_max()
    {
        counter="10";
        int x = Integer.parseInt(counter);
        for(int i=0; i<x; i++)
        {
            list.add(rand.nextInt(1000));
        }
        int i = Collections.max(list);
        return i;
    }
    
}
