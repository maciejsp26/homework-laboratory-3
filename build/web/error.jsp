<%-- 
    Document   : error
    Created on : 31 maj 2022, 15:45:26
    Author     : maciej
--%>

<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<f:view>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Error 404 =]</title>
    </head>
    
    <body>
    <h1><h:outputText value="Błąd logowania"/></h1>
        <h:form id="formularz_error">
            <h:outputText value="Podana nazwa użytkownika: #{logowanie.nazwa}
            jest nieprawidłowa, lub podano błędne dla niej hasło! " /><br />
            <h:commandButton value="Spróbuj ponownie" action="retry" />
        </h:form>
    </body>
</html>
</f:view>
