<%-- 
    Document   : baza
    Created on : 31 maj 2022, 16:55:58
    Author     : maciej0
--%>

<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Wyświetlanie danych</title>
        </head>
        
        <body>
            <h1><h:outputText value="Jesteś zalogowany jako: #{logowanie.nazwa}" /></h1>
            
            <h:form id="form2">
                <b>Wciśnij przycisk zalosuj by wolosować 10 liczb!</b>
                <h:commandButton value="Zalosuj" action="losowanie" /><br />
                <h:commandButton value="Wyloguj" action="wyloguj" />
            </h:form>

        </body>
    </html>
</f:view>