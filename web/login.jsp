<%-- 
    Document   : output
    Created on : 30 maj 2022, 19:22:52
    Author     : maciej
--%>

<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<f:view>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Strona logowania</title>
    </head>
    <body>
        <h1><h:outputText value="Formularz logowania"/></h1>
        <h:form id="formularz_login">
            <h:outputText value="Wprowadź nazwę użytkownika : " /><br />
            <h:inputText value="#{logowanie.nazwa}" /><br />
            <h:outputText value="Wprowadź hasło" /><br />
            <h:inputSecret value="#{logowanie.hasło}" /><br />
            <h:commandButton value="Zaloguj" action="#{logowanie.sprawdz}" />
        </h:form>
    </body>
    </html>
</f:view>