<%-- 
    Document   : result
    Created on : 1 cze 2022, 10:28:01
    Author     : maciej
--%>

<%@page import="jsf.obliczanie"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
        </head>

        <body>

            <h1>Wyniki:</h1>
            <jsp:useBean id="dBean" class="jsf.obliczanie" />
            <% obliczanie wynik = new obliczanie(); %>
            <b>Przykładowa lista: <%= wynik.losowanie() %></b><br />    <br />

            <b>Losowanie nr 1, element maksymalny: </b>
            <% obliczanie wynik2 = new obliczanie(); %>
            <b><%= wynik2.losowanie_max() %></b> <br />


               <h:form id="form2">
                <br />
                <b>Zalosuj ponownie !</b>
                <h:commandButton value="Zalosuj" action="losowanie" /><br />
                <h:commandButton value="Wyloguj" action="wyloguj" />
            </h:form>


        </body>
    </html>
</f:view>

    
        
        
    

